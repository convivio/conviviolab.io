# Convivio Cookbook

The Cookbook contains our recipes for running the company. It is the single source of truth on how we do things.]

The Cookbook pages contain the current finalised recipes. The [issue tracker in Gitlab](https://gitlab.com/convivio/convivio.gitlab.io/-/issues) contains all the discussion around these recipes, improving them, and adding new ones, as well as any questions.

All Convivio staff can directly edit the Cookbook, or submit merge requests.

The Cookbook is published at https://cookbook.convivio.com

---

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/),
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. Install Docusaurus:

   ```sh
   cd website
   yarn install
   ```

1. Preview your project:

   ```sh
   yarn start
   ```

   Your site can be accessed under http://localhost:3000.

1. Add content.
1. Generate the website (optional):

   ```sh
   yarn build
   ```

   The website will be built under `website/build/`.

Read more at the [Docusaurus documentation](https://docusaurus.io).
