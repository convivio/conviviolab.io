# Company details

Convivio is a 

#### Registration details

Registered Company Name: Convivio Team Ltd 

Registered Address: 27 Old Gloucester Street, London, WC1N 3AX, United Kingdom

Registered Number: 10213988 

Registration in: England & Wales 

Incorporation Date: 3rd June 2016 

Financial Year End: 31st March  (changed from 30th September in 2021)

Companies House records: [https://beta.companieshouse.gov.uk/company/10213988](https://beta.companieshouse.gov.uk/company/10213988)

#### Postal Address

This is the best address for all correspondence:  Convivio, 27 Old Gloucester Street, London, WC1N 3AX.

#### Owners and Directors

100% of our shares are owned by Steve Parks, a UK taxpayer. Correspondence address: Convivio, 27 Old Gloucester Street, London, WC1N 3AX.

We have one director, Steve Parks, a UK taxpayer. Correspondence address: Convivio, 27 Old Gloucester Street, London, WC1N 3AX.