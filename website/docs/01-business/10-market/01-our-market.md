
# Our Market

In order to succeed, we want a target market that:
- Has a clear pain
- We have the ability to solve that pain and enable them to achieve better things
- Has purchasing power
- We know how to reach
- Is growing
- We would feel fulfilled and happy in serving them

Our market is:
- Founders of creative & digital agencies, who...
- Are active in a director/CEO role
- Have 10-100-ish people in the agency
- Want to grow their business ambitiously
- Want to primarily build-to-own and independent business rather than build-to-sell (but that doesn't mean they'd say no to an amazing offer). If there are acquistions to be done, they'll more likely be the acquiror.
- Want to build a business they are proud of, and are motivated to lead
- Want to build a business that is respected and enduring 
- Want to build wealth, of which money is just one part. They also want quality of life, and deeper fulfilment.

Their pain points are:
- They feel overwhelmed by how much they need to do in their multiple roles
- They don't know where to focus their attention and energy for maximum impact
- They feel reactive, with their attention being pulled to the noisiest issues each day ("firefighting"), rather than being able to proactively design their business for the long term
- They feel isolated, with nobody else in the business understanding the responsibility that sits on their shoulders, or the unique set of problems they are juggling
- They feel directionless, and haven't had the opportunity or guidance to think about an outcome from al their work other than "We'll sell the agency, I guess"
- They feel pressure to be 'crushing it, smashing it', following hustle culture, rather than being comfortable leading the business with their natural personality and values
- They don't have experience running businesses and keep learning the lessons the hard way, which is expensive and stressful
- They network with other agency founders and that provides some emotional support and reassurance that , but mostly everyone is in the ssame boat and doesn't have answers


Our work will help them, so that they can:


Our experience in this market has shown us that this audience has the purchasing power, we know how to reach them.

We don't have data on whetehr the market is growing or not, but are very close to the action. We believe that the market is going through a consolidation period and is about to have another growth burst.

Our experiments in this market have shown that we find great fulfilment in helping this audience solve these particular problems.