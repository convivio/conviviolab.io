# Brand Guidelines

This page provides guidance for using the visual identity of our brand in practice.

## Logo

The logo comes in three variants: square, wide and mark only.

![Convivio Logo Variants](/img/Convivio-logo-variants.png)

Square and wide can be used in all situations and should just be chosen for best ﬁt based on the space available.

The mark only variant can be used in any situation where it isn’t essential to display the company name - like on t-shirts, other merchandise, and in presentation slides.

To make sure there’s always plenty of breathing room, there should be a space the size of the ‘vi’ around the logo on all sides.

![Convivio Logo Usage](/img/Convivio-logo-usage.png)

We favour using our logo subtly, and have actually asked that our logo be made smaller in a few different situations.

The logo comes in two colours. The red variant should be used when the logo is placed on a very light or very dark neutral coloured background.

![Red Logo Usage](/img/Logo-red-usage.png)

The cream variant should be used when the logo is placed on a bright coloured, or mid-shade neutral coloured background.

![Cream Logo Usage](/img/Logo-cream-usage.png)

The logomark can be made transparent and offset for a page background.

![Background Logo usage](/img/Logo-background-usage.png)

## Colours

export const Swatch = ({children, color}) => (
  <span
    style={{
      backgroundColor: color,
      padding: '4px 100px',
      minWidth: '100px',
      border: '1px solid black',
    }}>
    {children}
  </span>
);

### Primary Colour

**Jellybean**

* <Swatch color="#EC604B"> </Swatch>
* #EC604B 
* RGB: 236,96,75 
* CMYK: 0,74,68,0

### Secondary Colour

**Isabelline**
* <Swatch color="#F8F4EA"> </Swatch>
* #F8F4EA 
* RGB: 248, 244, 234 
* CMYK: 4,4,10,0

### Tertiary Colours

These colours round out the palette. They are bold and fresh to match the feeling of the picnic table.

**Accent: Rajah**

* <Swatch color="#FFD066"> </Swatch>
* #FFD066
* RGB: 254,208,104
* CMYK: 0,20,67,0

**Contrast: Keppel**

* <Swatch color="#45A8A3"> </Swatch>
* #45A8A3
* RGB: 68,169,164
* CMYK: 70,11,40,0

**Positive: Aquamarine**

* <Swatch color="#70D893"> </Swatch>
* #70D893
* RGB: 125,193,141
* CMYK: 56,0,56,0

**Relaxed: Lilac**

* <Swatch color="#C8A2C8"> </Swatch>
* #C8A2C8
* RGB: 200, 162, 200
* CMYK: 0,19,0,22

### Black, White and Grey

**Black: Jet**

* <Swatch color="#343434"> </Swatch>
* #343434
* RGB: 52,52,52
* CMYK: 69,59,56,65

**White: White**

* <Swatch color="#FDFDFF"> </Swatch>
* #FDFDFF
* RGB: 254,254,255
* CMYK: 1, 0, 0, 0

**Grey: Nickel**

* <Swatch color="#717070"> </Swatch>
* #717070
* RGB: 113,112,112
* CMYK: 0,0,0,55

## Typography

Galano Grotesque Bold is the Convivio brand typeface.

It should be used for the logo, names, and titles.

![Brand Typeface - Galano Grotesque](/img/Typography-Galano.png)

Avenir Next is the body font. Use it for paragraphs, quotes, and subtitles. We use Regular, Medium, Demi-Bold weights.

![Brand Typeface - Avenir Next](/img/Typography-Avenir.png)

## Print Usage

The visual identity can be used strikingly on a wide variety of print products.

![Brand print usage](/img/Brand-print-usage.png)

## Credits

Our visual identity was developed by the wonderful humans, Zach and Laura, at [Superhero Studios](http://superhero-studios.com/)

It was developed collaboratively with the whole Convivio team.