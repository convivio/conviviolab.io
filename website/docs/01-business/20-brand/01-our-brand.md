
# Our Brand

![Convivio Logo](/img/Wide-Red-Small.png)

## The name

Convivio is an Italian word capturing the feeling of being round a big table with family and friends, sharing good food and conversation.

It represents togetherness, nourishment and good times.

## The logo

![Convivio Logo Mark](/img/Mark-Only-Red-Small.png)

Our logo is a picnic bench.

This represents our casual, relaxed, fun and informal approach. Just like good food and good times don't need to be fancy and overcomplicated, so our work can be natural and human, without pretence.

Our logo symbolises the warm and collaborative atmosphere of Convivio.

## Brand guidelines

Our [brand guidelines](/business/brand/brand-guidelines) set out useful guidelines about our brand visual identity.