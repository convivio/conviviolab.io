# Healthy recruitment

Recruiting the wrong person is bad enough, but what if you’ve turned down exceptional candidates because your selection process failed? Job interviews are a staple of every recruitment process, but what do your interviews really tell you about the people you’re hiring, or how they’ll fit into your workplace?

That was exactly the problem we found ourselves with. Mostly we’ve known we had a problem because we’ve got to the end of an interview and felt that we hadn’t found out as much about the person as we should’ve done, and arranged a follow-up to ask more questions. From the outside it seemed like a professional multi-step process. From the inside we’ve known we could’ve done a better job.

Here, I’ll look specifically at the interviews themselves — how we conduct them and why we do things that way.

## Interviewing Backwards

Part of our problem was that our interviews were kinda backwards. We thought we were being all cool and natural, having our interviews in a pub, being chummy and very off-the-cuff with our questions, and so on. We’d ask questions like:

* ‘What are you looking for in this role?’
* ‘Tell us about a project where you had to deal with a challenging technical problem, and how you solved it.’
* ‘Tell us about a team you worked in that didn’t function well, and what you did to make it work better.’
* ‘What is your greatest weakness?’

But questions like that somehow never really helped us to understand the people we were interviewing or to make good decisions about how to proceed.

## Beyond Past Behaviour

After much reflection and discussion, the most significant change we’ve made is to the kind of questions we ask, and the way in which we ask them.

The main problem with our questions above is that they’re ‘past-behavioural interview’ (PBI) questions: they look back at past behaviour and assume that it forms a good predictor of future behaviour. Past-behavioural interview questions can be a powerful tool to discover a candidate’s ability and predict their performance. Research on their effectiveness indicates a good correlation between candidate responses in interviews and how they will actually perform in the workplace, explaining around 25–30% of the variance between candidates (Menkes, [Executive Intelligence](http://www.amazon.co.uk/dp/0060781882), 193). However, PBIs are also fundamentally flawed.

Interviews using these questions don’t explain the majority of the differences in performance between individuals. They’re simply not good at accounting for how complex human beings are.

Moreover, PBIs and past-behavioural questions ask the candidate what they did in the past and allow them to be selective about what they present to the interviewer. If the candidate has taken some time to think a bit about their working history, how to take parts of it and present them in a good light, then they’re fairly easy questions to answer.

## Knowing the Rules of the Game

For example, here’s a classic PBI question about interpersonal skills and conflict resolution:

> ‘Describe a situation where you had to interact with a difficult colleague and diffuse your interpersonal differences.’

An intelligent response would focus on an example that accentuates the conflict, highlights the candidate’s humility, their ability to be self-critical whilst still holding convictions and being able to convince and persuade, and emphasises a harmonious and humane outcome. But how much would a response like that actually tell you about how the candidate would act in the workplace?

PBIs are really measuring candidates on three factors: experience, job knowledge and social skills. These are all important things in a working environment, but they’re relatively easy for a candidate to fake by constructing impressive and self-flattering stories from a career history. In other words: it’s easier to be successful in an interview if you know the unwritten rules of the game.

What is more, those three factors are obviously more developed if you’ve had a longer career. Justin Menkes quotes Jon Miller, AOL’s CEO, who puts it this way (Menkes, Executive Intelligence, 207):

> “As someone who interviews a lot of potential employees in various capacities, I have a theory that if you’ve survived in business to a certain point, and done reasonably well, you probably present yourself well, have a good base of business knowledge, and you have a good story. Yet none of those things are enough to tell you whether someone is actually going to excel in your organisation or not. They’ve had a chance to hone their story; we all learn to do that over time. But how well someone tells their story is not enough.”

Miller understands that experience (which often dominates job interviews), job knowledge and social skills are all much more developed in candidates with a longer career. I’ll return particularly to social skills below.

PBIs are certainly useful and have value, especially in establishing the candidate’s baseline qualifications and capabilities. But they have a fundamental flaw: they do not measure how a person will act in your workplace.

(For more about past-behavioural interviews and their limits, see Justin Menkes, [Executive Intelligence: What all Great Leaders Have](http://www.amazon.co.uk/dp/0060781882), especially chs 11–12.)

## Biased Humans

There’s a bigger problem than just accounting for candidates with longer careers, though, and that’s to do with flawed human psychology. We suffer from cognitive and decision-making biases that skew our perceptions of people and the world. We think we’re being slow and deliberative as we make judgements and take decisions but we’re actually being dominated by lots of fast-brain approximations and biases without realising it.

## The Handshake Halo

First impressions are important, it’s well known. But that’s the start of the problem. Psychology has revealed that we make fast judgements of people when we first meet them, and those immediate gut-feel reactions act as a bias, shaping our subsequent judgements of them. It’s called the halo effect.

> ‘The halo effect is a bias in which our overall impression of a person (a figurative halo) colors our judgment of that person’s character.’ — beinghuman.org/article/halo-effect

These first-impression judgements, formed in as little as the first 7 seconds, can be incredibly strong and distort anything the person says or does afterwards.

This, it turns out, is a key reason our past-behaviour questions are so anodyne and unhelpful for us in working out whether a candidate would be successful here: our questions weren’t taking any account of the first-impression halo effect.

Candidates with better social skills create a strong positive halo effect, and the PBI-type questions we were asking couldn’t account for that.

## Anchors Aweigh

We are simultaneously dominated by anchoring, a cognitive bias where we humans tend to rely too heavily on the first piece of information offered when making decisions. That may be the first impression — a confident (or furtive) walk into the room, a firm (or limp) handshake, a broad (or nervous) smile — or it may be a great (or faltering) answer to the first question.

Once that first piece of information, the anchor, is set, other judgements are made by adjusting from that initial anchor and further information is determined around that anchor. A winning smile anchors to perceiving the candidate as confident, and will then colour the candidate’s answers through that filter of perceived confidence.

## Expecting Confirmation

Having formed that first impression and had our judgements anchored by it, it is easy to then suffer from confirmation bias. We human beings have a tendency to interpret and even search for evidence to confirm our already-held beliefs — we like to be proved right — whilst also diminishing the significance of evidence to the contrary. If a candidate speaks without hesitation, without umm-ing or err-ing, that may confirm a belief that they are confident, regardless of whether their answers to questions are coherent or appropriate.

## Interviewing Forwards: Structured Interviews
Justin Menkes suggests a different approach to interview questions (chs 13–15) that can better account for candidates’ experience, job knowledge and social skills, and also better predict whether a person will excel in your company. His alternative method tries to distinguish between knowledge, which is gained through experience, and intelligence, a more innate characteristic.

His approach is based in a conversational format, but uses a series of questions designed to examine the ‘cognitive aptitudes’ that we identify as central to the role. The idea is to pose small realistic scenarios that the candidate will have to use specific cognitive skills to solve.

This approach is known generically as ‘structured interviews,’ though it involves more than just giving structure to interviews.

Here is an example.

## Competing Priorities
You want to find out how well candidates for your delivery manager vacancy will be able to prioritise tasks and competing importances. A past-behavioural interview would commonly ask something like this:

> ‘Tell me about a time when you had to do several things at once. How did you handle the situation? How did you decide what to do first?’

That’s an easy question to answer, especially if you have worked for long enough to have a number of work experiences to draw on. A smart candidate might answer something like this:

> ‘I just had to be very organized. I had to multitask. I had to prioritize and delegate appropriately. I checked in frequently with my boss.’

That’s a good solid answer. It exploits the unwritten rules of the interview because it signals a number of traits that would be valuable in the workplace. But does it really reveal much about the way the candidate would actually act in the workplace? Instead, it could be rephrased into a scenario that would require the interviewee to solve a problem.

> ‘You’re in a situation where you have two very important responsibilities that both have a deadline that is impossible to meet. You cannot accomplish both. How do you handle that situation?’

This requires a completely different set of skills from the previous question. The candidate needs to analyse the situation, offer a solution to the problem, draw some conclusions and justify their reasons. Importantly, these questions don’t stand alone: they need a number of follow-up questions to investigate the answer given.

For example, if a candidate might answer something like:

> ‘Well, I would look at the two and decide what I was best at, and then go to my boss and say, ‘It’s better that I do one well than both poorly,’ and we’d figure out who else could do the other task.’

(taken from Malcolm Gladwell, [The New-Boy Network](http://gladwell.com/the-new-boy-network/))

… then you are be able to use that as an essential insight:

> ‘So, you’re a bit of a solo practitioner, are you?’

… and that should lead on to 2 or 3 further follow-up questions. These follow-up questions probably should be prepared in advance, at least the first ones, so that you’re equipped to respond to the range of likely answers.

Interviews involve around 4 or 5 of these sorts of scenario questions, tailored to the skills needed for the role in the vacancy and create the structure of these ‘structured interviews’. The next step in the structure is that the same scenario questions should be asked to all candidates for the same job, so that interviewees can be more easily compared. The third step in the structure is that the scenario questions act as a springboard to ask the more penetrating follow-up questions based on the candidate’s responses. The fourth step is to take good, detailed notes to examine and compare candidates afterwards

So, how have we put this into practice ourselves?

## Our Interviews
The scenario questions we ask candidates are based around our company values:

* doing the right thing: being healthy, sustainable and working with integrity;
* doing things right: being professional, achieving excellence;
* doing things big: being ambitious, thinking higher and long-term;
* doing things: getting things done with focus and momentum;
* doing things together: being collaborative and sharing, radically transparent.

For example, if interviewing for a developer, we might ask a question like this on the principle of doing things right:

> It’s two days before the end of a sprint and it’s obvious that not all stories will be completed. You and all your team have been working very hard for the whole sprint but nonetheless some crucially important functionality will not be delivered. What do you do?

Depending on the answers given, that then opens up to follow-up questions on estimating workload, handling heavy workloads, saying no to senior colleagues or clients, over-committing, work-life balance and so on. (Hopefully you can see how this question is a riff on the original above on competing priorities.) The point is to open up a conversation around the scenario, to find out what the person would be like in our workplace.

We take as many notes as reasonable, and that usually means several people participating. (I’ve used a three-column spreadsheet in the past to help in the interview — 1st column with the scenario questions; 2nd with various possible follow-up questions; 3rd for notes.) In part, we are looking to raise some red flags, some warnings of things we would need to take account of when working with this person — what support they might need, ways they prefer to work that would need to be facilitated, what they might be like in front of a client or when backed into a metaphorical corner, and so on.

Only when we feel like we know whether the candidate would thrive in our workplace and whether we can help them to excel here, will we consider taking them on. That way, we can support them better in their work and simultaneously create a healthy and happy workplace and project teams.




Much of how we’ve reworked our interviewing process has been influenced by an essay by [Malcolm Gladwell](http://gladwell.com/), [The New Boy Network](http://gladwell.com/the-new-boy-network/), originally [published in The New Yorker](http://www.newyorker.com/magazine/2000/05/29/the-new-boy-network) in 2000 and later collected into a book of essays, [What the Dog Saw (And Other Adventures)](http://www.amazon.co.uk/dp/0141044802), (London: Penguin, 2009).

Large parts of why we’ve reworked our interviews have been inspired by Justin Menkes, [Executive Intelligence: What all Great Leaders Have](http://www.amazon.co.uk/dp/0060781882) (London and New York: Harper Business, 2006).

On the process of hiring for a remote team, we were also very influenced by Wade Foster’s Zapier guide [The Ultimate Guide to Remote Work](https://zapier.com/learn/the-ultimate-guide-to-remote-working/) ([eBook online](https://zapier.com/learn/the-ultimate-guide-to-remote-working/)), especially Chapter 3, ‘[How to Hire a Remote Team](https://zapier.com/learn/the-ultimate-guide-to-remote-working/how-to-hire-remote-team/)’.