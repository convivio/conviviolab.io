---
title: Welcome
slug: /
sidebar_position: 0.1
sidebar_label: Welcome
---

# The Convivio Cookbook

This is where we store all the recipes for running Convivio.

It's where we store the 'single source of truth' about our company and how we do our work. It's the first place to refer to when you want to know something.

Our discussions around improving the company and the way we work are all then based around changing what's in the Cookbook.


You can read more about the Cookbook, including how to edit and contribute to it at [Tools>Cookbook](/tools/cookbook).


And yes, there are actual recipes in this cookbook! Look in the [Feasts section](/feasts)