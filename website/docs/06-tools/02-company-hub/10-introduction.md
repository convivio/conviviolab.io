
# Introduction

Convivio is a Free-Range company, which means we try to do as much as possible without needing to be in the same place (even a videocall) at the same time. It's asynchronous geographically-dispersed work

To help with this We have a Company Hub. This is where we put forward ideas and proposals, share information, and make decisions.

Our Company Hub is simply an issue queue in a GitLab repository.

Convivio team members can access the Hub at: https://gitlab.com/convivio/company/-/issues

# Why

Tools like Slack scroll by too fast and aren't great for slow consideration and thoughtful discussions. By having our updates, discussions and decisions in a more forum-like place it means anyone can catch up anytime and can contribute when others aren't around too.


## Viewing latest activity

The Company Hub is part of our internal ecosystem, including the Cookbook, and sits under the Convivio 'group' om Gitlab.

You can view all the latest activity here: https://gitlab.com/groups/convivio/-/activity


## Viewing things to know and work on

Anything that needs your attention is created as an 'issue'. You can see a list of all current issues at Convivio here: https://gitlab.com/groups/convivio/-/issues

This list can be filtered to find just the issues assigned to you, or just those with certain labels.


## Company-wide Labels on Issues

We use labels to tag and sort all the discussions and items that make up the issues list. There are different types of label:

- Channel (prefixed with #) - we link all our comms across different channels by these tags, eg these map to Slack channels and Google Docs folders.
  - #Convivio-Business
  - #Convivio-Services
  - #Convivio-Clients
  - #Convivio-Marketing
  - #Convivio-Team
  - #Convivio-Tools
  - #Social

- Services (prefixed with !) - A label for each of the services we provide to clients and we need to work on delivering
  - !Agency Leaders Podcast
  - !Agency Newsletter
  - !Agency Radar
  - !Agency Senate
  - !Board Dedicated
  - !Board Shared
  - !Board Virtual
  - !Briefings
  - !Consulting
  - !Convivio Membership
  - !Convivio Website
  - !Cookbook
  - !Course
  - !Office Hours
  - !Playbook

- Type (prefixed with *) - A label to indicate what 'kind' of issue this is, and therfore what's expected of people seeing it
  - *Convivio History: Just building up records for our archive, no action needed
  - *Decision-ToMake: Raising an issue that we all need to decide on, and seeking input and advice
  - *Decision-Made: Once the decision has been made the label is switched to this, and it's just for info to anyone who sees it now
  - *Idea: Recording an idea we might want to 
  - *Information: Just an update to keep people in the loop, no action needed
  - *Intention: Informing everyone that you intend to do something, and giving them a chance to give input and advice before you do. But otherwise you'll just go ahead.
  - *Problem: Raising a puzzle that we have to solve together. Inviting everyone else to collaborate in exploring and solving it.
  - *Proposal: 
  - *Questions
  - *Request
  - *Weeknotes

- Priority (prefixed with +) - A label to show if this is different to just normal work
  - +High (Flagging that we need to do this above all else)
  - +Someday Maybe (Flagging that this is just a long long term aspiration or idea and we don't need to do anything, just dream. It's for keeping track that the idea exists even though we din't want to work on it yet. Useful for gathering info over the course of months or years until the time is right.)

Each category of label is colour coded, as well as having the prefix, so they are easy to differentiate.

Each issue is likely to have a few of these labels to categorise it, possibly one or two of each category.

In addition, feel free to create other useful labels (but avoid the prefixes used above of #, !, *, +). You could create a label for a project such as 'project-projectname' for example. Or a label that's just useful for your own purposes. Anyone can create labels anytime. We can always merge or delete them later if needed.

There are two types of labels:
1. Group labels: These are available to use in all projects under the Convivio company 'group' in Gitlab.
2. Project labels: These are available to use only in that specific project in Gitlab.

In future (someday maybe) we will look at automating things a little based on labels applied.

# Issue Status

There are only two statuses for issues:

- Open: We need to work on this, or it's in progress
- Closed: It's done!

Any other status can be dealt with through labels, including custom labels.
