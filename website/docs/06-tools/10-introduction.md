# Introduction

We use a lot of software tools in our work. Here's a rundown of the key ones...
## Core Tools

All Convivio team members need to be set up on, and familiar with:

* [Cookbook](/tools/cookbook): This site, where you can find out everything you need to know
* [Google Workspace](/tools/google-workspace): Email, Calendar, Collaborative Documents, File Storage
* [Gitlab](/tools/gitlab): The company hub, plus contributing to this cookbook.
* [Slack](/tools/slack): Chat and synchronous collaboration
* [1Password](/tools/1password): Password management
* [Pleo](/tools/pleo): Company cards and expenses
* [Timetastic](/tools/timetastic): Annual and sick leave booking and reporting
* [Zoom](/tools/zoom): Video calls, and our phone system
* [Miro](/tools/miro): Collaborative online whiteboard

Other useful tools we use are:

## Content publishing, courses & marketing

* [Ghost](/tools/ghost): CMS for managing the Convivio website and core member services
* [MeetEdgar](/tools/social-media/scheduling/meetedgar): Content publishing for our social media channels
* [Missinglettr](/tools/social-media/scheduling/missinglettr): Drip content publishing for our social media channels
* [Canva](/tools/canva): Graphic design tool
* [Typeform](/tools/typeform): Form and survey tool
* [Teachable](/tools/teachable): Online course hosting

## Client support & marketing

* [Hubspot](/tools/hubspot): Customer relationship management (CRM) system
* [Shopify](/tools/shopify): Online store for selling standalone items
* [Stripe](/tools/stripe): Payments processor
* [Eventbrite](/tools/eventbrite): Live events sales and booking
* [Mailchimp](/tools/mailchimp): Outbound emails for marketing and member automations
* [Calendly](/tools/calendly): Meeting booking system

## Video and audio production

* [Descript](/tools/descript): Collaborative video and audio editing
* [Mmhmm](/tools/mmhmm): Add slides and other useful tools to Zoom calls and recorded videos
* [Transistor](/tools/transistor): Podcast hosting platform
* [Camo](/tools/camo): App for using your iphone as a camera for video calls and recording
