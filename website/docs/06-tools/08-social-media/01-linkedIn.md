

# LinkedIn

We have a company page on LinkedIn at: https://www.linkedin.com/company/convivio-agency

Steve and Joe are both admins and can make any changes or add you to the page.

## Posting

We post on LinkedIn following our [Social Media Plan](/marketing/content-strategy/social-media) under our Content Strategy.

## Network and Relationship building

We've also found that LikedIn is the social network that our audience of agency leaders uses most, so it's one of the primary ways we grow our network and build relationships with people we might be useful to.

## Tools

To help us publish regularly and easily to LinkedIn we use [Scheduling](/tools/social-media/scheduling) tools
