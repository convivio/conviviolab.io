
# Introduction

The Cookbook is where we store the 'single source of truth' about how we run Convivio. It's the first place to refer to when you want to know something. Our discussions around improving the company and the way we work are all then based around changing what's in the Cookbook.

All Convivio staff have full access to edit and create content in the Cookbook.


## Editing Cookbook pages

On each page there's a link at the bottom to 'Edit this page'. If you're a staff member, logged in to GitLab, this takes you straight to an editor for the page in Gitlab where you can edit the new content and commit it or create a merge request.

The repository can be found at: https://gitlab.com/convivio/cookbook 




