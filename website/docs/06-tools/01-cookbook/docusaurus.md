
# Docusaurus CMS

The Cookbook is built with the Docusaurus static CMS.

This is an open source project from the Meta/Facebook development team. It's specifically designed for hosting documentation sites.

You can find out more about Docusaurus:

* [Docusaurus documentation](https://docusaurus.io/docs)
* [Docusaurus Github repo](https://github.com/facebook/docusaurus)
* [Docusaurus issue queue](https://github.com/facebook/docusaurus/issues)