
# MDX formatting

In addition to Markdown, the Cookbook supports the MDX format for more advanced content.

You can read about MDX at:

* [the MDX site](https://mdxjs.com/)
* [the Docusaurus help site](https://docusaurus.io/docs/markdown-features/react)
