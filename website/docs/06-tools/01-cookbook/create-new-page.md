
# Create a new page

Create a new file in the right place in the file structure.

If you want to influence where the page appears in the sidebar menu, put a number and hyphen at the front of the filename. The site strips out these numbers to create the pathnames and titles. This is optional for pages, it's fine to leave it out.

Use the file extension '.md' normally, unless you know you want to use MDX (a cross of markdown and JSX), in which case use the extension '.mdx'

At the top of the new file, you can (optionally) put this frontmatter:

```md title="pagename.md"
---
title: The full title of the page
sidebar_label: Shortname
sidebar_position: 2
sidebar_class_name: classname
tags:
  - Food
  - Drink
---

# The title to display at the top of the page

Your text goes here

```

All lines are optional. It's fine to leave them out and rely on the defaults. Here they contain dummy demo data.
