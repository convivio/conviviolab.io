# Routing

Routing in Ghost is how we can set content as being in distinct sections, and available on certain paths.

For example, we can make pages be in the 'Boards' section of the 'Playbook'.

Each time we add a new category we'll need to update the routing. This is because we're pushing Ghost beyond it's intended purpose as a simple blogging/newsletter platform.

You can read more in the ([Ghost docs](https://ghost.org/docs/themes/routing/)).

## Download the routes YAML file

Start by downloading the current route `routes.yaml` from https://convivio.ghost.io/ghost/#/settings/labs — click the 'Download current routes.yaml' link in the Routes pane.

![Screenshot of Ghost Routing settings](/img/Screenshot_2022-09-05_at_18.52.png)

Then you'll need to edit that file in a text editor.

### Mug up on routes

Before starting this you need to know a little (not much) about the Ghost routing system (see docs link above). In particular, the difference between '[collections](https://ghost.org/docs/themes/routing/#collections)' and '[channels](https://ghost.org/docs/themes/routing/#channels)'. In essence:

- Collections are the core of organising pages in Ghost. From the Ghost docs:
    
    > **Collections serve two main purposes:**
    > 
    > 1. To display all posts contained within them on a paginated index route
    > 2. To determine the URL structure of their posts and where they ’live’ on the site. For this reason, posts can 
only ever be in **one** collection.

    Think taxonomies in Drupal.

- Channels are more flexible than collections … but means they can result in some ambiguity. From the Ghost docs:
    
    > A channel is a custom stream of paginated content matching a specific filter. This allows you to create subsets and supersets of content by combining or dividing existing posts into content hubs.
    >
    > Unlike collections, channels have no influence over a post’s URL or location within the site, so posts can belong to any number of channels.
    >
    > **The best way to think of channels is as a set of permanent search results.** It’s a filtered slice of content from across your site, without modifying the content itself.
    
    All the Convivio.com playbook sections are channels.

So, you'll see in the Convivio.com site `routes.yaml` file 

- there's general routes, some of which are designated as channels
- and, separately (at the bottom, line 266), there's collections

The file is quite big (319 lines currently), but that means you'll find lots of examples of current routes that you can emulate.

It should be said that the routes are 'read' by the system from top to bottom, i.e. the first thing it reads that fulfils the criteria is where a page sits.

That means more for collections (coz a page can only be in one collection) than it does for channels, but it's just worth bearing in mind.

## Worked example

Here we're gonna take items given the 'agency-board' tag and present on https://www.convivio.com/tag/agency-board/ and put them into a new section in the playbook, at `/playbook/agency-board/`

So, if you search for 'okrs' in the file you'll find this, creating the channel for the `/playbook/okrs/` playbook section:

```yaml
  /playbook/okrs/:
    permalink: /playbook/okrs/{slug}/
    filter: primary_tag:playbook+tags:okrs
    order: posts_tags.sort_order ASC, featured DESC, published_at DESC
    template: collection-index-playbook
    data: tag.okrs
    controller: channel
```

All you need to do is copy that, paste it somewhere else in the vicinity (I would do it immediately above, as the bit after that is for the playbook OKRs sub-sections), and then alter it like this:

```yaml
  /playbook/agency-board/:
    permalink: /playbook/agency-board/{slug}/
    filter: primary_tag:playbook+tags:agency-board
    order: posts_tags.sort_order ASC, featured DESC, published_at DESC
    template: collection-index-playbook
    data: tag.agency-board
    controller: channel
  /playbook/okrs/:
    permalink: /playbook/okrs/{slug}/
    filter: primary_tag:playbook+tags:okrs
    order: posts_tags.sort_order ASC, featured DESC, published_at DESC
    template: collection-index-playbook
    data: tag.okrs
    controller: channel
```

You'll notice that there's changes on:

- the first line, which acts as the element in the YAML file
- the permalink
- the filter
- the data

Note also the indents — they need to match.

Done that? Ready to go!

## Upload your routes file

Just use the 'Upload routes YAML' button on https://convivio.ghost.io/ghost/#/settings/labs — the upload will trigger Ghost to register updated file and update is register of the routes.

Your new channel should then be available at https://www.convivio.com/playbook/agency-board/