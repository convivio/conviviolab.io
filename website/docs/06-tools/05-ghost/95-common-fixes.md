# Common Fixes

## Theme CSS files seem not to load on certain pages / Resetting Theme

Joe investigated an incident of this and thinks this can happen when there's a Ghost deploy and a page gets requested before Ghost has managed to build out all the assets, and then the page request gets cached. Or something like that.
He fixed it by …

1. navigating to https://convivio.ghost.io/ghost/#/settings/design/change-theme
2. Our theme is currently on banquet (banquet-2.8.11) — because our theme version uses Semantic Versioning (SEMVER), but Ghost's list doesn't sort properly on version numbers, it's weirdly in the middle of the list, that goes …

  - banquet (banquet-2.8.1)
  - banquet (banquet-2.8.10)
  - banquet (banquet-2.8.11)

3. 'Activate' the previous version (i.e. banquet-2.8.10) — when you click to 'activate' there's no indication that something's happening until it reloads
4. Re-'Activate' the current version (i.e. banquet-2.8.11)

Theme gets reloaded, styles all back