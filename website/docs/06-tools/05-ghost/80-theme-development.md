# Theme Development

The Convivio website uses a customised Ghost theme. This page sets out how to work on that theme.

## Setting up for local development

Here's how to get set up with a local development installation of Ghost, ready for working on the development of our website theme.

### Set up the tools

1. Install Git
2. Install [Visual Studio Code](https://code.visualstudio.com/)
3. Install Visual Studio Code command line tool (In VSC press F1 for the command palette. Type 'shell command' and select the option to install code command in PATH)
4. Install [Ghost extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=TryGhost.ghost)
5. Install Gscan, a tool for validating themes: ```npm i gscan -g```

### Install Ghost CLI

1. Follow the instructions to [install Ghost CLI](https://ghost.org/docs/install/local/), including its dependencies
2. Continue to follow the instructions there to install Ghost locally. Do that in a sensible directory for you to access the files, eg ~/development/convivio-ghost. In the rest of these instructions we'll refer to this director as convivio-ghost.
3. The command line output will give you a URL to access the development site. 
4. Check the site is working

### Import the Convivio content

1. In our live website, go to [Settings>Labs](https://convivio.ghost.io/ghost/#/settings/labs). Next to 'Export your content' click Export and save the file locally
2. In your local development version of Ghost go to Settings>Labs and click 'Open Importer'. Select the file you just downloaded from the live site
3. After a few minutes check the frontend of the site to see that the content is now being shown (although the theme won't be right yet)

### Install the Convivio Ghost theme

1. Ensure you have access to the [Github repo](https://github.com/ConvivioTeam/Convivio-Ghost-Banquet-Theme).
2. Ensure your [Github profile has SSH keys](https://github.com/settings/keys) installed, or follow the [instructions](https://docs.github.com/en/authentication/connecting-to-github-with-ssh) to set them up
3. In Terminal, in the convivio-ghost directory, change to the themes directory: ```cd content/themes/```
4. Clone the git repo for the theme into a new directory named 'banquet': ```git clone git@github.com:ConvivioTeam/Convivio-Ghost-Banquet-Theme.git banquet```
5. Change to the new banquet directory: ```cd banquet```
6. Install dependencies: ```npm i```
7. Restart the ghost instance, so it discovers the new theme: ```ghost restart```
8. Build the theme's static files: ```npm run dev```
9. In the ghost admin user interface go to Settings>Design. In the bottom left click 'Change Theme'. In the top right click 'Advanced'.
10. Click the 'Activate' button next to Banquet
11. Go to the local site front end to check the theme is being used

### Get Visual Studio Code set up

1. Open Visual Studio code in the banquet directory: ```code .```
2. From the 'Terminal' menu, choose 'Open New Terminal'. In normal development this is where you can issue the commands you'll need. But for now they are already running in the original terminal.
2. You can choose to set up a workspace in VSC to save your configuration.

### When finished

Once everything is set up and you're ready to stop:

1. Return to the original terminal and press CTRL-C to stop the npm dev process watching the theme files
2. Run ```ghost stop``` to stop the Ghost local server
3. Close the terminal, and close VSC.

Thats it, you're all set for any future work on the Convivio theme. You don't need to do the steps above each time. Just start from 'Working on the theme' below.


## Working on the theme

### Getting ready for development

1. Open Visual Studio Code, choosing to open up the workspace or directory you had before.
2. Use VSC's Source Control tab to Pull or Sync the latest version of the git repository
3. In the VSC terminal type ```cd .../.../.../``` then ```ghost update``` to update the Ghost install if there is a new version available. Then type ```cd content/themes/banquet``` to return to the theme directory. 
3. In the VSC terminal type ```npm i``` to update any dependencies
4. In the VSC terminal type ```ghost start```, copy the URL it shows you
5. In the VSC terminal type ```npm run dev```. That will build static copies of the theme files and watch for any changes you make in development, so the development site will always reflect your edits
6. In your browser, navigate to the URL that the Ghost Start command gave you.
7. If it's been a while since you last used this install, or if there are very recent content changes you want to work on in the theme, you may want to reexport the content from the production site and reimport it to this local install. See 'Import the Convivio content' section above.

You're now ready to work on the theme.

### Start a new branch

The main branch of the git repo is protected, meaning merges can only be made to it via pull request from another branch. So any changes you make need to be made in another branch.

1. In VSCode's Source Control tab, select the 'Branches' vertical slider at the bottom.
2. Click the + to create a new branch, using a naming format: ```feature/some-feature-name-here```, such as ```feature/add-playbook-sidebar```

OR

1. In the terminal, while in the main branch ```git branch -m feature/some-feature-name-here```

Now you're ready to make your edits, and view them in your local dev site.

### Deploy your changes

Once you're happy with the changes:

1. Use VSCode's Source Control tab to commit them to your feature branch (you should also do this part regularly while you are making changes, to have more atomic commits)
2. Run Gscan, either from the VSCode command palette typing 'Gscan', or in the terminal with ```npm run test```. Fix any issues that arise and commit any changes.
3. Increment the version number in the package.json file (at the time of writing, for example "version": "2.8.12")
4. Run ```npm run build``
5. That creates a new Zip file for you, named for the version number The file is created in a dist folder in the banquet theme root (folder is made if it doesn’t exist already), so a build for v2.8.12 will be at ./dist/banquet-2.8.12.zip
6. Upload this zip file to the live Convivio site at [Settings>Design>Change Theme](https://convivio.ghost.io/ghost/#/settings/design/change-theme) and activate it. Check everything works as expected on the live site.
7. Do a final commit of changes to your branch
8. Sync or Push to push the changed branch to the remote (or can do this in the Terminal with ```git push -u origin feature/some-feature-name-here```)
9. Go to the Github repo to [create a new pull request](https://github.com/ConvivioTeam/Convivio-Ghost-Banquet-Theme/pulls) from your branch to main
10. Review the Pull Request and merge it to main
11. Back in VScode, switch to the main branch and do a Sync or Pull to ensure your main branch is up to date again

And now your changes have been made, committed to the codebase and deployed to the live site

### Finishing up

Once your work is done and you're ready to stop:

1. In the VSCode terminal, press CTRL-C to stop the npm dev process watching the theme files
2. Run ```ghost stop``` to stop the Ghost local server
3. CLose the terminal, and close VSC.
4. All done!

## Advanced steps

### Set up mail sending
In rare circumstances you may need to set up your local installation to be able to send emails for you to test, or to send you password resets.

In that case, follow these instructions (but thhey're normally not needed)

1. Go to https://myaccount.google.com/apppasswords
2. Enter your password for your Google account
3. Select app, choose Other, and put the name (i.e "Ghost local dev install")
4. Copy the displayed password and save it somewhere (you won't be able to view it again at Google). NOTE: it may copy with spaces in it, remove the spaces so you just have 16 characters.
5. In the VSCode terminal for your theme folder, enter ```code ../../../config.development.json``` to open the config file for editing
6. Replace the existing "mail" section with the code shown below, changing the details for your email and password:

```
"mail": {
    "transport": "SMTP",
    "options": {
      "host": "smtp.gmail.com",
      "port": 587,
      "auth": {
        "user": "your@google.email",
        "pass": "the app password you created"
      }
    }
  },
```

## Other useful information

- [The essential concepts of Ghost theming](https://ghost.org/tutorials/essential-concepts/)
- [The Convivio Ghost Theme Git repository](https://github.com/ConvivioTeam/Convivio-Ghost-Banquet-Theme) (Includes a helpful readme)