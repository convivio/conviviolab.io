# Introduction

Slack is a chat app for teams. We use it for chatting when there's a few of us around.

Our Slack instance is at https://weareconvivio.slack.com.

Slack is just like bumping into people you work with and having a chat. You should assume that nobody sees anything you post in Slack. If you need somebody/everybody to see something, post it in the [Company Hub](/tools/company-hub).

We also use Slack for posting non-urgent notifications from all of our various apps.

It's a place to see the company activity scroll by when you happen to be in work.


## Channels

- #Convivio-Business
- #Convivio-Services
- #Convivio-Clients
- #Convivio-Marketing
- #Convivio-Team
- #Convivio-Tools
- #Social-News
- #Social-Reading
- #Social-Random

