---
title: Transistor
slug: /tools/transistor
sidebar_label: Transistor
---

# Transistor

[Transistor](https://transistor.fm/) is a podcast hosting service. We use it to publish and distribute the public Convivio Agency Leaders Podcast, and the members only podcasts too.

## Integration with Ghost

Transistor has some level of [integration with Ghost](https://ghost.org/integrations/transistor/).