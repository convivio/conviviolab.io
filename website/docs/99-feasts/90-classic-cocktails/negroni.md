# Negroni

The first time you try one of these it’s mouth-puckering, but if you like a savoury drink before a big meal it’ll quickly become a favourite. 

If you're new to Campari, this cocktail can seem too bitter at first, so you can reduce the Campari a little until you get into the swing of things. But these measures are perfect, so do work up to it.

### Ingredients

* 1 measure gin
* 1 measure red vermouth
* 1 measure Campari
* A strip of orange peel for garnish
* Lots of ice

### Method

1. Fill a tumbler with ice
2. Pour in each measure of drink, stirring it well until it is chilled, and adding more ice if necessary, before adding the next measure
3. Take some orange peel. Cut into an attractive shape, spritz over the drink and slide into the glass

### Options

* Try replacing the gin with bourbon. This is known as a Boulevardier 
