# Martini

This is the simplest of all cocktails, but makes for a perfect aperitif before a rich dinner.

To enjoy this drink it’s best to experiment and find where you are on our Martini scale. Start at the dry end, and gradually add more vermouth until your tastebuds zing. Then make a note and make it like that in future — but experiment again once in a while because you may well find your tastes get a little drier over time.

### Ingredients

* Gin (roughly in measures shown on sliding scale below)
* White Vermouth (roughly in measures shown on sliding scale below)
* Strip of lemon peel
* Ice

|   |  Driest &lt;&lt;- | &lt;- | Perfect | -&gt; | -&gt;&gt; Less Dry |
| - | --- | - | ------- | - | ----- |
| Gin | 2 | 2 | 2 | 1.5 | 1 |
| Vermouth | Wave bottle near glass | 0.5 | 1 | 1 | 1 |

### Method

1. Fill the serving glass with ice and a little water, to chill the glass while you mix the drink
2. Fill a mixing glass with ice cubes. Pour over the gin and stir until the gin is well chilled.
3. Add the white vermouth and stir well again.
4. Tip the ice and water out of the serving glass, and strain the drink from the mixing glass into the serving glass.
5. Peel the zest from a lemon and trim to be an attractive strip. Slide down the edge of the glass into the drink
6. Take another piece of lemon peel and spritz over the glass

### Options

You can experiment with jazzing up your martini with any one of these:

* Add an olive and some of the olive brine for a ‘dirty’ martini.
* Add a dash of orange bitters
* Add a sprig of rosemary and dash of balsamic vinegar (!!)
