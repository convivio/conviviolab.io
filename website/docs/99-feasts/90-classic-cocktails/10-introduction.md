
# Introduction

Convivio is an Italian word that captures the feeling of being round a big table with family and friends sharing good food and good conversation.

Think of those evenings where everyone has brought something to the table - a dish of something, a bottle, some decorations, or just the willingness to wash up — and there are candles on the table, lights strung above it, and maybe a fire nearby.

When we’re celebrating, we like to start those kind of evenings with a cocktail to get the tastebuds singing and build an appetite, and then maybe something more mellow after dinner to enjoy by the fire. Over time we’ve collected our team’s favourites, perfected them, and we share them here.

When it comes to cocktails we’re fans of the classics. Great flavours, minimum faff — and no umbrellas (unless it's raining).

That means you can make all of our favourites with a small range of ingredients, and no fancy kit.

* [Ingredients](/feasts/classic-cocktails/ingredients)
* [Tips and Tricks](/feasts/classic-cocktails/tips)

### The recipes

* [Martini](/feasts/classic-cocktails/martini)
* [Negroni](/feasts/classic-cocktails/negroni)
* [Manhattan](/feasts/classic-cocktails/manhattan)
* [Old Fashioned](/feasts/classic-cocktails/old-fashioned)