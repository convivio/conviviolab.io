
# Introduction

## 1. Meet

## 2. Engage

In this step we want people to sign up for our free service plan. This is effectively signing up for our email newsletter, but in time we may add extra benefits.

The aim is that they can see our way of thinking, and gain some immediate value from us with no risk.

We find that some people just want to see a few editions to know there's a good fit with what they need, and others can subscribe for months or even a year before being ready. Some won't ever move on from this. That's fine. The aim is to give enough value that people can decide whether they have a need for more.

To get people to sign ip for our free service plan, we take the following steps:

- We include a call to action (CTA) for the newsletter in all presentations we give on webinars or at events
- We inclide a CTA in our email footers
- We include a CTA in our blog posts on the site
- We regularly schedule social posts to promote that we have the newsletter
- We set up social campaigns for each newsletter issue, directing to it's page on convivio.com 
- In all cases we direct people to convivio.com/newsletter


When people sign up for our free service plan, there are the following steps:

1. They sign up at convivio.com, powered by Ghost
2. Ghost sends them a confirmation double opt in email
3. They click on the link in this email, which takes them to the Convivio.con/welcome
4. This page shows them a reminder of what they get
5. Ghost syncs their details with Hubspot CRM
6. Ghost syncs their details with Mailchimp
7. Mailchimp starts an automation 'Convivio Newsletter sign-up: welcome email'
8. The automation tags them as 'Convivio Newsletter Subscriber'
9. The utomation waits and hour, and then sends them the welcome email from Mailchimp
10. They receive the welcome email which lets them know the first issue will come on the following Monday and includes a few examples of previous editions that have been popular.

They will then receive the weekly newsletter by email directly from Ghost. They also now have a login to our Ghost instance which we can use to provide them with other exclusive content in future.

## 3. Inform and inspire

## 4. Convert

## 5. Serve