
# Social Media

We want to approach social media with the same core principles as anything else in our content strategy:

- Be human
- Be useful


## Types of Post

### Manual posts


### General updates

These are content-focused updates.

#### Blog posts


#### Newsletters

Before publication tease what's coming with a link to the newsletter sign up page.

On the day of publication schedule a post at around the time of publication.

Set up an ongoing campaign for the post in Missinglettr.

### Agency Radar

### Promotional



## Community

It's important that we don't only use social media in 'transmit' mode. We're part of a community and we want to see what others are posting, boost other people's posts where appropriate, and join in with the conversation with them. Again, be guided by: Be human, be useful.
