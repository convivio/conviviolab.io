
# Expenses

In the course of your work you'll need to spend money on behalf of the company.

For any travel, accomodation and food as part of travelling for work, you don't need any approval.

For other expenditure below £100 you don't need approval.

We just ask that you spend the company's money responsibly, because it benefits all of us if the company is financially solid.

For amounts over £100, please get peer-review from another colleague.


## Company cards

We prefer team members not to have to spend their own money and then wait to claim it back. Therefore every member of the team gets a company debit card.

These cards are powered by [Pleo](/tools/pleo), and are paired with a mobile app. Whenever you spend money, the Pleo app or website automatically creates an expense report for that item. You just need to take a photo of the receipt in the app, or attach it in the website, and that's it!

It is a requirement to upload an image of an official full VAT receipts for all expenditure.


## Cash expenses

If you do incur cash expenses out of your own pocket, or have to use your own card for something, you can reclaim that money quickly and easily.

Again, the [Pleo](/tools/pleo) app is used for this. In the mobile or web app, select the button to add an expense manually.

Once the expense is added, the 'Pocket' menu item will show the amount owed to you. You can have this balance sent directly to your own account.

This means you can be reimbursed for out of pocket expenses within an hour.

For more information on using this, please see the Pleo help page on [How to submit a pocket expense and get reimbursed](https://help.pleo.io/en/articles/5207155-how-to-submit-a-pocket-expense-and-get-reimbursed).

## Mileage

Please take public transport wherever possible, as part of our climate policy. But where you absolutely have to drive, you can claim mileage expenses at the current maximum rate allowed by HMRC (45p/mile at the time of writing)

Mileage claims can also be submitted through the Pleo app. Use the + button in the top right of the screen, and select 'mileage'.
