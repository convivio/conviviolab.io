# Tools

## Core Tools

All Convivio team members need to be set up on, and familiar with, 

* [Cookbook](/tools/cookbook): This site, where you can find out everything you need to know
* [Google Workspace](/tools/google-workspace): Email, Calendar, Collaborative Documents, File Storage
* [Gitlab](/tools/gitlab): The company hub, plus contributing to this cookbook.
* [Slack](/tools/slack): Chat and synchronous collaboration
* [1Password](/tools/1password): Password management
* [Pleo](/tools/pleo): Company cards and expenses
* [Timetastic](/tools/timetastic): Annual and sick leave booking and reporting
* [Zoom](/tools/zoom): Video calls, and our phone system
* [Miro](/tools/miro): Collaborative online whiteboard

For a complete guide to our tools, see the [Tools section](/tools) of the cookbook.