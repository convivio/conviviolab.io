# Peer reviews

Many things that happen at Convivio require a decision to be taken.

To help us take decisions that match with our values — open and transparent, sharing and consultative, putting people first, looking after ourselves and others, looking long-term, and so on — we have adopted a process of _peer review_ for decision-making.

**Our predisposition is to be supportive and peer requests are met with enthusiasm and an eagerness to help, but they are a time for reflective evaluation and aren't just a rubber stamp.** 

## What kind of thing gets a peer review?

Many things should go through the peer review process. Here are some examples:

* **Holiday and time-off**
  * We have a very flexible holiday policy. However, to prevent that approach being abused, both through people not taking enough holiday or taking too much, holiday requests need a peer review.
  * These should be evaluated by the people most affected, usually (but not necessarily) your direct colleagues on your curren projects.
* **Equipment, tools and services**
  * When signing buying a new piece of hardware, an app for your work or an ongoing service, generally speaking, a peer review should be requested.
  * However, if the purchase cost is small, under £100, you probably don't need to bother. (Often, though, if you're not asking for a peer review it's polite to simply flag the purchase. Apart from anything, other people may have recommendations to share)
* **Conferences and training**
  * Continuous professional development is good, healthy and important for everyone — for you, for your colleagues, for clients and for the company. We want you to go to conferences and to have training, through online or offline courses. However, because these will mean you won't be doing other work while you're there, requests for these should be peer reviewed.

This is not an exhaustive list and other things besides may also need a peer review. The rule of thumb is, if it will affect others in the company in some way, or if it is a large outlay, get a peer review. Otherwise just let people know your intention.

## Asking for a peer review

Post an issue in the [Company Hub](/tools/company-hub) to start a peer review.

Here's some rules of thumb for your request:

* **DO be detailed.** In order for someone to give an effective review they need to properly understand what you're asking for and why. Give enough detail for someone to evaluate your request.
* **DO expect questions.** Rule Zero of peer reviews is that they are not just a public announcement that will automatically get a green light. This is not a rubber stamping process. You may be asked question so reviewers are sure they understand the context, key factors, knock-on effects etc.
* **DO keep asking if no one's answering.** If you haven't had a review for your request, usually it's just busyness that means they haven't seen the request. If necessary, @name a person you think is most likely to give an appropriate review — someone from your team, for example.

## Reviewing a request

The key part of peer review requests is the review by your peers. Don't be daunted, though — reviewing a peer request is pretty simple.

1. **Review the request.** Sounds obvious, but all that means is _take some time to think_. Don't just answer straight away. We're all nice people, and our instinct is to help out and say 'yes'. However, it's important to override our instincts and to be a bit analytical. Take a moment to review what's been asked for, think it through and how it might impact on other things.
2. **Ask some questions.** No peer review should just zip through. Each should be examined for its merits, and that means asking questions. You should ask _at least one_ probing question, to understand the reasons behind a request. 
3. **Seek clarity.** If you're not clear what's being asked, or the justification is a bit muddy, ask again.
4. **Ask for help.** If you can't decide by yourself, or the request requires a decision for something bigger than one person should judge alone, ask for some else to help you out.
5. **Make a clear decision.** Once you've come to a decision to approve or reject a request, make it clear to the requester and anyone else reading.
6. **Take action.** Once you've made a decision, if necessary, record the consequence somewhere. E.g. a holiday or time-off request should have an entry added into [Timetastic](/tools/timetastic).

Anyone in the company can respond to a peer review request. However, you should bear in mind some important rules of thumb:

* **DO get involved.**

  If you see a request, try to offer a review.

* **DO remember it is a** _**peer**_ **review.**

  The first reviewers should be most affected by the request, if that's appropriate. For example, time away from a current client engagement — for holiday, to attend a conference or training, or other time off — it is the members of the engagement team who are most affected, so they are the peers who need to do the reviewing.

* **DO get others involved.** Often, and especially with large expenditures, requests may need more than one person to evaluate them. If you need someone to help you with a review, just ask for help from a seconder, or even a thirder.
* **DON'T ignore them.** Requests need an answer. Although some requests may be outside your ability to answer, if you're not a true peer because you're on a different project, for instance, you can still get involved. Make it clear to the asker that the request has been seen, and flag it to someone who can better answer the request.
* **DON'T leave it to someone 'more senior.'** Peer reviews are not just for 'management' oversight. They are for us _all_ to be open and transparent, supportive and facilitating. Anyone can review a peer request, even one that may involve spending a substantial amount of money. Just follow the simple step-by-step guide above.
* **DON'T be shy of saying no.** Not every peer request needs to be approved. Although we try to be accommodating and supportive as much as possible, there are times when 'no' is the necessary answer.
* **DON'T be arsehole.** Be nice.
