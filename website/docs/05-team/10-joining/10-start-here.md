# Start here

Welcome to Convivio!

The early days in a new organisation can be daunting and can leave you feeling lost. We've put together a reference guide to help you get started on your journey.

When you start with us we like to make sure you receive all of the hardware and access you need to do your job. You'll also need some guidance to help you find your way.

We keep a checklist of everything you need to make sure we don't miss anything but here's a quick guide to get you started.

## You get a buddy

When you first start you'll be assigned a buddy. We're a friendly bunch but you'll find it easier if there's a single person you can turn to to help you out initially.

Find out more about [having a buddy](/team/joining/having-buddy).

## Hardware and budget

You'll need a laptop as a minimum to get you started, and one should already have been provided. If you've not already got one then let your main contact, your buddy, or the CEO know.

All members of staff receive a hardware budget. You decide how to spend this money provided it's for work. The budget will cover a high-spec laptop (expected to last 3 years), a mobile device, and various extra useful things for our free range working.

## Payday

Shortly after starting (or possibly just before) you'll be asked for some personal information that we'll need to pass on to our accountants for payroll.

## Expenses

We provide everyone with a company card, connected to a mobile app, that makes managing expenses very easy. Find out more in [Claiming Expenses](/team/hr-handbook/expenses)

## Fridays

Fridays are special days. We don't book any calls, meetings or events, and focus instead on deeper creative work, experiments, and professional development.

## Holidays

We have an unlimited holiday policy here at Convivio. It takes a bit of getting used to, but it's an important factor of making sure we are all properly energised for the work we do.

We have a peer review approach to booking holidays, but our peer review protocol means we also check people are taking _enough_ time off and not being martyrs to the work.

See [How to Book Holidays](/team/hr-handbook/annual-leave) for more information.

## Peer review

Most things that involve a decision are handled by [a process of peer review](/team/ways-of-working/peer-review). We use peer review to help evaluate everything from buying an app or product, taking out a subscription to a service, requesting holiday, choosing where and when to have our retreats and more.

## Communication Tools

All of our team is [free range](/team/ways-of-working/free-range), so effective communication is vitally important. With the right behaviours supported by useful tools we can be sure of good, clear communication for everyone.

This [Cookbook](/tools/cookbook) is the 'single source of truth' about how the company works. Any team member can contribute to it and update existing pages. Discussing changes and improvements in the company happens by way of discussing changes to the Cookbook, in the Company Hub.

The [Company Hub](/tools/company-hub)is where we have all the key company dicussions, and manage internal projects and collaboration. This allows us to work together asynchronously.

We use [Slack](/tools/slack) for most of our communications, so it's a good idea to have this installed and open most of the time. With free range working, people can't just look over to see what you're working on, so we have developed a habit of 'working out loud', using Slack channels, messages, statuses, and more to work expressively and vocally.

Many of our project and system tools are integrated with Slack, so as projects progress our project and company management tools, user research tools, code management review processes and system monitoring tools, for example, all keep us informed of how things are progressing.

We rely on video conferencing tools for more human conversation and talking in more detail. We use [Zoom](/tools/zoom) wherever possible, but sometimes need to use other platforms for external meetings and events.

We use a company Gmail account for all email correspondence. Your email address will be firstname.surname@convivio.com.

## Health & Safety

Due to the distributed nature of the team it is not practical to visit your home office on a regular basis. The company needs you to assess your own comfort and safety and to communicate any risks or discomfort to management. Where the risk or discomfort is related to hardware or furniture then we'll work with you to fix this.

When you begin working with the organisation we'll have already asked you about your home office arrangements to determine whether or not you have a suitable work space in which to perform your job. Some people work solely from home, others choose to split their time between home and co-working spaces.

Travel is a necessity for most of us to perform our jobs. The company does not operate or deal with customers in potentially risky countries. We only travel and operate in destinations approved by the UK Foreign Office.

If you have any issues or questions related to Health and Safety these must be discussed with your project or team lead or the CEO.

## Everything Else

If you still have questions, look further around the Cookbook or ask for help in Slack — we'll always answer.
